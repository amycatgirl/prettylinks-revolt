async () => {
  /**
   * @type {import("revolt.js").Client}
   */
  const client = await new Promise((res) =>
    setInterval(() => {
      if (window.controllers.client.getReadyClient) {
        res(window.controllers.client.getReadyClient());
      }
    }, 5000)
  );

  /**
   * @param {string} link - URL to fetch website title
   **/
  async function extractTitleFromWebsite(link) {
    try {
      const title = await fetch(`https://api.allorigins.win/get?url=${link}`)
        .then(async res => {
          const text = await res.json()
          console.log(text)
          return text
        })
        .then(html => {
          const parser = new DOMParser()
          const dom = parser.parseFromString(html.contents, "text/html")
          return dom.querySelectorAll("title")[0].innerText;
        });
      return title
    } catch (e) {
      console.error("Error while parsing title", e);
    }
  }

  console.log("[debug/prettylinks-revolt] Ready!");

  /**
   * @param {import("revolt.js").Message} message
   */
  async function FormatAndSendMessage(message) {
    const SHOULD_EDIT = message.author._id === client.user._id;

    if (SHOULD_EDIT)
      console.log("[debug/revolt-prettylinks] Trying to edit message...");

    const NEED_CONVERSION = SHOULD_EDIT
      ? message.content.match(HTTP_REGEX)
      : null;

    const LINK_LIST = NEED_CONVERSION
      ? NEED_CONVERSION.filter((item) => !item.match(SPEC_REGEX))
      : null;

    if (!NEED_CONVERSION || !LINK_LIST || !SHOULD_EDIT) return;

    console.log("[debug/revolt-prettylinks] Found links", LINK_LIST);

    // Check whether links don't follow the APL spec and store them so that they don't get checked
    const CONVERTED = await Promise.all(
      LINK_LIST.map(async (item) => {
        const url = new URL(item);
        const host = url.hostname;
        const title = await extractTitleFromWebsite(item);

        console.log("[debug/revolt-prettylinks] Converting link", item);
        const conv = `[[${host}: ${title}]](<${item}>)`;
        console.log(conv);

        return conv
      })
    );

    // Split the contents of the message and check through them
    const FIXED_MESSAGE = message.content.split(" ").map((part) => {
      if (LINK_LIST.includes(part)) {
        const INDEX = LINK_LIST.findIndex((link) => link === part);

        const conv = CONVERTED[INDEX];
        console.log("dev:", conv, INDEX);
        return conv;
      }

      return part;
    });

    await new Promise((res) => {
      const interval = setInterval(() => {
        console.log(
          "[debug/revolt-prettylinks] Editing message with contents",
          FIXED_MESSAGE.join(" ")
        );
        res(message.edit({ content: FIXED_MESSAGE.join(" ") }));

        clearInterval(interval);
      }, 1000);
    }).then(() => console.log("[debug/prettylinks-revolt] Done!"));
  }

  const HTTP_REGEX =
    /((http|https):\/\/)?(www[0-9]\.)?(([A-Za-z0-9_-])+\.{1})+([A-Za-z]{2,4}|\<[^<>]+\>)(\/([A-Za-z0-9_-])+)*(\/)?/gm;
  const SPEC_REGEX =
    /\[\[(www[0-9]\.)?(([A-Za-z0-9_-])+\.{1})+([A-Za-z]{2,4}|\<[^<>]+\>)(\/([A-Za-z0-9_-])+)*(\/)?: [A-z]+\]\]\(<((http|https):\/\/)?(www[0-9]\.)?(([A-Za-z0-9_-])+\.{1})+([A-Za-z]{2,4}|\<[^<>]+\>)(\/([A-Za-z0-9_-])+)*(\/)?>\)/gm;

  client.on("message", async (message) => await FormatAndSendMessage(message));

  return {
    onUnload: async () => {
      console.log("[Plugin] Unloading prettylinks-revolt");
      client.removeListener("message", async (message) =>
        await FormatAndSendMessage(message)
      );
      console.log(
        "[Plugin] Plugin prettylinks-revolt should be unloaded correctly."
      );
    },
  };
};
