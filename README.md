# Amy's Pretty Links for Revolt

A revite plugin that automatically formats sent links using the APL spec

## Installing

Get the latest release from the `plugin.json` file on the "Releases" tab

You can start using the plugin by following these steps:

1. Enable the "Experimental plugin API" experiment on settings
2. Open your browser's devtools (Using CTRL+SHIFT+I)
3. Navigate to the console tab and paste the following:

```js
// Include the contents of plugin.json inside of the parenthesis
state.plugins.add(<here!>)
```

## Contributing

You can help with development of this plugin by opening issues, pull requests and fixing bugs.

### Requirements

- `node@18.17.1` or later
- `pnpm@8.6.12` or later

### Building

Run `pnpm build` to make a production build to the `target` directory or `pnpm build:dev` to generate a development build of the plugin.

> #### Note
>
> Development builds use the `-devel` suffix, loading the plugin won't unload the
> Production build

## Spec

[[codeberg.org: amycatgirl/amyprettylinks-webextension: Copy APL-formatted links from your browser! - amyprettylinks-webextension - Codeberg.org]](<https://codeberg.org/amycatgirl/amyprettylinks-webextension>)

## Credits

- `ERROR 404: NULL NOT FOUND` - Plugin Idea
- `Lea` - Revite plugin boilerplate

`generate-plugin.js` is a modified version of
[[github.com: GitHub - sussycatgirl/revite-ts-plugin-boilerplate]](<https://github.com/sussycatgirl/revite-ts-plugin-boilerplate/tree/master>)
